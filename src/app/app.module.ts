import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { UserProfileComponent } from './user-profile/user-profile.component';

const config = {
  apiKey: 'AIzaSyCYG5ucwlyaBMaPXicQyb4JGu7gVme53BE',
  authDomain: 'remedy-86fc3.firebaseapp.com',
  databaseURL: 'https://remedy-86fc3.firebaseio.com',
  projectId: 'remedy-86fc3',
  storageBucket: 'remedy-86fc3.appspot.com',
  messagingSenderId: '501394013694',
  appId: '1:501394013694:web:b17e609b81313e1b3c54af',
  measurementId: 'G-MK3HYLH6VP'
};
@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule,
    AngularFireAuthModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  declarations: [AppComponent, UserProfileComponent]
})
export class AppModule { }
